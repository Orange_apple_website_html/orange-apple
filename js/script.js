 function resize(){
		function changeEvenHeight(){
		var aHeight = $('#main-about section.card div.left').outerHeight();
		var aHeight2 = $('#main-about section.card div.right').outerHeight();
		if (aHeight > aHeight2){
			$('#main-about section.card div.right').outerHeight(aHeight);
		} else {
			$('#main-about section.card div.left').outerHeight(aHeight2);
		}
	}

	changeEvenHeight();	

	function equalWidget1(){
        var h = 0;
        $('#main-services ul li div.bottom').height('auto');
        $('#main-services ul li div.bottom').each(function(){
            var height = $(this).height();
            if(height > h){
                h = height;
            }
        });
        $('#main-services ul li div.bottom').height(h);
    }
    equalWidget1();

    function equalWidget1(){
        var h = 0;
        $('#main-services ul li div.top img').height('auto');
        $('#main-services ul li div.top img').each(function(){
            var height = $(this).height();
            if(height > h){
                h = height;
            }
        });
        $('#main-services ul li div.top img').height(h);
    }
    equalWidget1();

	changeEvenHeight();	

	function card(){
	  $('#counselling .counselling-card ul li').outerHeight('auto');
		  	var height = 0;
		  	$('#counselling .counselling-card ul li').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#counselling .counselling-card ul li').outerHeight(height);
  	}
  	card();

  	function card9(){
	  $('#counselling .counselling-card ul li div.bottom').height('auto');
		  	var height = 0;
		  	$('#counselling .counselling-card ul li div.bottom').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#counselling .counselling-card ul li div.bottom').outerHeight(height);
  	}
  	card9();

  	function card10(){
	  $('#counselling .counselling-card ul li div.top').height('auto');
		  	var height = 0;
		  	$('#counselling .counselling-card ul li div.top').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#counselling .counselling-card ul li div.top').outerHeight(height);
  	}
  	card10();

  	function card1(){
	  $('section#topics section.topics-card ul li').outerHeight('auto');
		  	var height = 0;
		  	$('section#topics section.topics-card ul li').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('section#topics section.topics-card ul li').outerHeight(height);
  	}
  	card1();

  	function card2(){
	  $('#topics .topics-card ul li div.bottom-card div.bottom').outerHeight('auto');
		  	var height = 0;
		  	$('#topics .topics-card ul li div.bottom-card div.bottom').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#topics .topics-card ul li div.bottom-card div.bottom').outerHeight(height);
  	}
  	card2();

  	function card3(){
	  $('.inner-courses #courses .courses-card ul li').height('auto');
		  	var height = 0;
		  	$('.inner-courses #courses .courses-card ul li').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('.inner-courses #courses .courses-card ul li').outerHeight(height);
  	}
  	card3();

  	function card4(){
	  $('.inner-courses #courses .courses-card ul li div.top').height('auto');
		  	var height = 0;
		  	$('.inner-courses #courses .courses-card ul li div.top').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('.inner-courses #courses .courses-card ul li div.top').outerHeight(height);
  	}
  	card4();

  	function card5(){
	  $('.inner-courses #courses .courses-card ul li div.bottom').height('auto');
		  	var height = 0;
		  	$('.inner-courses #courses .courses-card ul li div.bottom').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('.inner-courses #courses .courses-card ul li div.bottom').outerHeight(height);
  	}
  	card5();

  	function card6(){
	  $('#consulting .consulting-card ul li').height('auto');
		  	var height = 0;
		  	$('#consulting .consulting-card ul li').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#consulting .consulting-card ul li').outerHeight(height);
  	}
  	card6();

  	function card7(){
	  $('#consulting .consulting-card ul li div.bottom-card div.top').height('auto');
		  	var height = 0;
		  	$('#consulting .consulting-card ul li div.bottom-card div.top').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#consulting .consulting-card ul li div.bottom-card div.top').outerHeight(height);
  	}
  	card7();

  	function card8(){
	  $('#consulting .consulting-card ul li div.bottom-card div.bottom').height('auto');
		  	var height = 0;
		  	$('#consulting .consulting-card ul li div.bottom-card div.bottom').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#consulting .consulting-card ul li div.bottom-card div.bottom').outerHeight(height);
  	}
  	card8();

  	function card11(){
	  $('#topics .topics-card ul li div.bottom-card div.bottom h4').height('auto');
		  	var height = 0;
		  	$('#topics .topics-card ul li div.bottom-card div.bottom h4').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#topics .topics-card ul li div.bottom-card div.bottom h4').outerHeight(height);
  	}
  	card11();

  	function card12(){
	  $('.inner-courses #courses .courses-card ul li div.bottom h3').height('auto');
		  	var height = 0;
		  	$('.inner-courses #courses .courses-card ul li div.bottom h3').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('.inner-courses #courses .courses-card ul li div.bottom h3').outerHeight(height);
  	}
  	card12();

  	function card13(){
	  $('#counselling .counselling-card ul li div.bottom-card div.bottom h4').height('auto');
		  	var height = 0;
		  	$('#counselling .counselling-card ul li div.bottom-card div.bottom h4').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('#counselling .counselling-card ul li div.bottom-card div.bottom h4').outerHeight(height);
  	}
  	card13();

  	function card14(){
	  $('.inner-consulting #consulting .consulting-card ul li div.bottom-card div.bottom h4').height('auto');
		  	var height = 0;
		  	$('.inner-consulting #consulting .consulting-card ul li div.bottom-card div.bottom h4').each(function(){
			  	var $this = $(this);
			  	var h = $this.outerHeight();
		  		if (h > height){
			  		height = h;
		  		}
	  	});
	  	$('.inner-consulting #consulting .consulting-card ul li div.bottom-card div.bottom h4').outerHeight(height);
  	}
  	card14();


}
$(document).ready(function(){
	resize();
  // $(".owl-carousel").owlCarousel({
  //   "nav" : true,
  //   "loop" : true,
  //   "margin" : 40
  //  });
  	$('.sidecard').click(function(){
		$(this).find('.sidecard').toggle();
		$('.services').slideToggle();
	});
	$('.gallerycard').click(function(){
		$(this).find('.gallerycard').toggle();
		$('.gallery').slideToggle();
	});
	
  	$('span.menu-icon').click(function(){
  		$('header div.mobile-menu').slideDown("slow",function(){
  			$('span.menu-close-icon').show();
  		});
  	});

  $('span.menu-close-icon').click(function(){
  	$('header div.mobile-menu').slideUp("slow",function(){
  		$('span.menu-close-icon').hide();
  	});
  });


  var owl = $("#slider");
  	owl.owlCarousel({
  		items:3,
  		nav : true,
  		slideSpeed : 300,
  		controlls : true,
  		paginationSpeed : 400,
  		loop : true,
		margin : 40,
  		responsive : {
		// breakpoint from 0 up
		0:{
            items:1
        },
        480:{
        	items:1
        },
        648:{
            items:2
        },
        680:{
        	items:2
        },
        980:{
            items:3
        },
        1000:{
            items:3,
        }
	}
  	});


  // $('#main-services ul li div.bottom a.button').hover(function(){
  // 	$('#main-services ul li div.bottom a.button').css("background","#00ACC1")
  // });

});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}



$(window).load(function(){
	resize();
});

$(window).resize(function(){
	resize();
});
